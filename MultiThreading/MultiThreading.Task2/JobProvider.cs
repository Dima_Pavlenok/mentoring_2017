﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task2
{
    internal class JobProvider
    {
        private Thread _thread;
        private CancellationTokenSource _cts;

        public void DoThreadJob(Action job)
        {
            _thread = new Thread(() => job()) { IsBackground = true };
            _thread.Start();
        }

        public void DoThreadPoolJob(Action<CancellationToken> job)
        {
            _cts = new CancellationTokenSource();
            ThreadPool.QueueUserWorkItem(t => job(_cts.Token));
        }

        public Task<long> DoTaskFactoryJob(Func<CancellationToken, long> job)
        {
            _cts = new CancellationTokenSource();
            return Task.Factory.StartNew((token) => job((CancellationToken)token), _cts.Token);
        }

        public Task<long> DoTaskJobAsync(Func<CancellationToken, long> job)
        {
            _cts = new CancellationTokenSource();
            return Task.Run(() => job(_cts.Token), _cts.Token);
        }

        public void StopThreadJob()
        {
            _thread?.Abort();
        }

        public void StopTaskJob()
        {
            _cts?.Cancel();
            _cts?.Dispose();
        }
    }
}