﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MultiThreading.Task2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int MaxLevel = 25;

        private int _selectedValue;

        private readonly Random _rnd;
        private readonly JobProvider _jobProvider;
        private readonly List<Method> _methods = new List<Method>
        {
            new Method { Id = 1, Name = "With Thread" },
            new Method { Id = 2, Name = "With Thread Pool" },
            new Method { Id = 3, Name = "With Task" },
            new Method { Id = 4, Name = "With async/await" }
        };


        public MainWindow()
        {
            InitializeComponent();

            _rnd = new Random();
            _jobProvider = new JobProvider();

            Methods.ItemsSource = _methods;
            Methods.SelectedItem = _methods.First();

            Status.Text = string.Empty;
            Start.IsEnabled = true;
            Stop.IsEnabled = false;
        }

        private async void Start_OnClick(object sender, RoutedEventArgs e)
        {
            OperationStarted();

            try
            {
                switch (_selectedValue = (int)Methods.SelectedValue)
                {
                    case 1:
                        _jobProvider.DoThreadJob(() => OperationCompletedSuccessful(FactorialHardWork(_rnd.Next(MaxLevel), CancellationToken.None)));
                        break;
                    case 2:
                        _jobProvider.DoThreadPoolJob((token) =>
                        {
                            try
                            {
                                OperationCompletedSuccessful(FactorialHardWork(_rnd.Next(MaxLevel), token));
                            }
                            catch (Exception ex)
                            {
                                OperationCompletedWithException(ex);
                            }
                        });
                        break;
                    case 3:
                        var task = _jobProvider.DoTaskFactoryJob(token => FactorialHardWork(10, token));
                        task.ContinueWith(x => OperationCompletedSuccessful(x.Result), TaskContinuationOptions.NotOnFaulted);
                        task.ContinueWith(x => OperationCompletedWithException(x.Exception), TaskContinuationOptions.OnlyOnFaulted);
                        break;
                    case 4:
                        Status.Text = (await _jobProvider.DoTaskJobAsync(token => FactorialHardWork(_rnd.Next(MaxLevel), token))).ToString();
                        OperationCompleted();
                        break;
                    default:
                        Status.Text = "There is no this method!";
                        break;
                }
            }
            catch (Exception ex)
            {
                OperationCompleted();
                Status.Text = ex.Message;
            }
        }

        private void Stop_OnClick(object sender, RoutedEventArgs e)
        {
            switch (_selectedValue)
            {
                case 1:
                    _jobProvider.StopThreadJob();
                    Status.Text = "The operation was cancelled";
                    break;
                case 2:
                case 3:
                case 4:
                    _jobProvider.StopTaskJob();
                    break;
                default:
                    Status.Text = "There is no this method!";
                    break;
            }

            OperationCompleted();
        }

        private long FactorialHardWork(int x, CancellationToken token)
        {
            long result = 1;

            for (var i = 1; i <= x; i++)
            {
                token.ThrowIfCancellationRequested();

                result *= i;
                Dispatcher.Invoke(() => ProgressBlock.Value = (100 * i) / x);
                Thread.Sleep(1500);
            }

            return result;
        }

        private void OperationCompletedSuccessful(long result)
        {
            Dispatcher.Invoke(
                () =>
                {
                    Status.Text = result.ToString();
                    OperationCompleted();
                });
        }

        private void OperationCompletedWithException(Exception ex)
        {
            Dispatcher.Invoke(
                () =>
                {
                    Status.Text = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                    OperationCompleted();
                });
        }

        private void OperationCompleted()
        {
            Start.IsEnabled = true;
            Stop.IsEnabled = false;
            ProgressBlock.Value = 0;
        }

        private void OperationStarted()
        {
            Stop.IsEnabled = true;
            Start.IsEnabled = false;
            Status.Text = string.Empty;
        }
    }
}