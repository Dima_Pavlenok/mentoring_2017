﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Dialog();
        }

        private static void Dialog()
        {
            Console.WriteLine("Input upper limit: ");
            var upperLimit = GetValue();

            while(true) 
            {
                try
                {
                    using (var cts = new CancellationTokenSource())
                    {
                        var counterTask = CountAsync(upperLimit, cts.Token);

                        Console.WriteLine("If you want to change upper limit, please enter another value: ");
                        upperLimit = GetValue();

                        if (!counterTask.IsCompleted)
                        {
                            cts.Cancel();
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
            }
        }

        private static Task CountAsync(long upperLimit, CancellationToken token)
        {
            return Task.Run(() => Count(upperLimit, token), token);
        }

        private static void Count(long upperLimit, CancellationToken token)
        {
            Thread.Sleep(4000);

            long result = 0;
            for (var i = 0; i <= upperLimit; i++)
            {
                if (token.IsCancellationRequested) return;

                result += i;
            }

            Console.WriteLine($"Result: {result}. Input upper limit: ");
        }

        private static long GetValue()
        {
            long upperLimit;
            while (!long.TryParse(Console.ReadLine(), out upperLimit) || upperLimit <= 0)
            {
                Console.WriteLine($"The upper limit is not correct! Please, input correct value.{Environment.NewLine}");
            }

            return upperLimit;
        }
    }
}